#!/usr/bin/env python
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# Inspirated from:
# http://www.modmypi.com/blog/raspberry-pi-gpio-sensing-motion-detection
#
# This python program register PIR senses movements and send notifications
# to Pushbullet.
#
# Requirements:
# * RPi.GPIO
# * pushbullet.py
# * export PUSHBULLET_API_KEY
#
# Get the Pushbullet API key here:
# https://www.pushbullet.com/#settings/account

import httplib, urllib
import RPi.GPIO as GPIO
import time

token = os.environ['PUSHOVER_TOKEN']
user = os.environ['PUSHOVER_USER']
PIR_PIN = 11

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(PIR_PIN, GPIO.IN)

conn = httplib.HTTPSConnection("api.pushover.net:443")

while True:
    i = GPIO.input(PIR_PIN)
    if i == 0:
        print "No motion detected", i
        time.sleep(5)
    elif i == 1:
        print "Motion detected", i
        conn.request("POST", "/1/messages.json",
          urllib.urlencode({
            "token": token,
            "user": user,
            "message": "Motion detected!",
          }), { "Content-type": "application/x-www-form-urlencoded" })
        conn.getresponse()
        conn.close()
        time.sleep(5)
